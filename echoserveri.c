#include "csapp.h"
void echo(int connfd);
void sigchld_handler(int sig);
int main(int argc, char **argv)
{
 int listenfd, connfd;
 unsigned int clientlen;
 struct sockaddr_in clientaddr;
 struct hostent *hp;
 char *haddrp, *port;
 if (argc != 2) {
  fprintf(stderr, "usage: %s <port>\n", argv[0]);
  exit(0);
 }
 port = argv[1];
 Signal(SIGCHLD, sigchld_handler);
 listenfd = Open_listenfd(port);
 while (1) {
  clientlen = sizeof(clientaddr);
  connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
  /* Determine the domain name and IP address of the client */
  hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
    sizeof(clientaddr.sin_addr.s_addr), AF_INET);
  haddrp = inet_ntoa(clientaddr.sin_addr);
  printf("server connected to %s (%s)\n", hp->h_name, haddrp);
  if (Fork() == 0) {
   Close(listenfd);
   echo(connfd);
   Close(connfd);
   exit(0);
  }
  Close(connfd);
 }Close(connfd);
 exit(0);
}
int get_popen(char *command){
    FILE *pf;
    char data[512];
    pf = popen(command,"r");
    // Error handling
    // Get the data from the process execution
    fgets(data, 512 , pf);
    // the data is now in 'data'
    if (pclose(pf) != 0){
        fprintf(stderr," Error\n");
        return -1;
    }
    fprintf(stdout, "Se ejecuto correctamente\n ");
    return 1;
}
void echo(int connfd)
{
 size_t n;
 char buf[MAXLINE];
 int response;
 rio_t rio;
 Rio_readinitb(&rio, connfd);
 while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
  printf("server received %lu bytes\n", n);
  printf("\ncomando:%s",buf);
  response = get_popen(buf);
  printf("%d",response);
  if(response == -1){
    write(connfd, "-1", 3);
  }else{
    write(connfd, "1", 3);
  }
  Rio_writen(connfd, buf, n);
 }
}
void sigchld_handler(int sig) {
 while (waitpid(-1, 0, WNOHANG) > 0);
 return;
}

